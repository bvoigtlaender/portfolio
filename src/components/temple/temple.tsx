import { useEffect, useRef } from 'react';
import { HemisphereLight, Mesh, MeshPhongMaterial, PerspectiveCamera, Raycaster, Scene, Vector2, Vector3, WebGLRenderer } from 'three';
import { lerp } from 'three/src/math/MathUtils.js';
import { createCube, createPlane, createTexture, loadTemple } from './3d';
import type { MutableRefObject } from 'react';
import type { Renderer } from 'three';

interface TempleProps {
}

export default function Temple({ }: TempleProps) {
  const targetVector: MutableRefObject<Vector3> = useRef(new Vector3(0, 5, 40));
  const targetRotation: MutableRefObject<Vector3> = useRef(new Vector3(0, 0, 0));
  let temple: Scene
  let camera: PerspectiveCamera
  let renderer: Renderer
  let mouse = new Vector2()
  let raycaster = new Raycaster()
  const ANIMATION_SPEED = .1;

  // handle key press
  useEffect(() => {
    const handleKeyPress = (event: KeyboardEvent) => {
      switch (event.key) {
        case 'a':
          targetVector.current = new Vector3(-20, 5, 0)
          targetRotation.current = new Vector3(0, -Math.PI * .5, 0)
          return true;
        case 'd':
          targetVector.current = new Vector3(20, 5, 0)
          targetRotation.current = new Vector3(0, Math.PI * .5, 0)
          return true;
        case 's':
          targetVector.current = new Vector3(0, 5, 20)
          targetRotation.current = new Vector3(0, 0, 0)
          return true;
        case 'w':
          targetVector.current = new Vector3(0, 5, -20)
          targetRotation.current = new Vector3(0, -Math.PI * 1.5, 0)
          return true;
        default:
          return false;
      }
    }

    document.addEventListener('keydown', handleKeyPress)
    return () => document.removeEventListener('keydown', handleKeyPress);
  }, [])

  // load scene
  useEffect(() => {
    camera = new PerspectiveCamera(30, window.innerWidth / window.innerHeight, 0.1, 100);
    const canvas = document.getElementById('temple') as HTMLCanvasElement
    temple = new Scene();

    const planeSize = 40;
    const texture = createTexture(planeSize);
    const plane = createPlane(texture, planeSize);
    temple.add(plane);

    const mesh1 = createCube(4, 'Cube1');
    mesh1.position.set(20, 0, 0);
    temple.add(mesh1);
    const mesh2 = createCube(4, 'Cube2');
    mesh2.position.set(-20, 0, 0);
    temple.add(mesh2);
    const mesh3 = createCube(4, 'Cube3');
    mesh3.position.set(0, 0, 20);
    temple.add(mesh3);
    const mesh4 = createCube(4, 'Cube4');
    mesh4.position.set(0, 0, -20);
    temple.add(mesh4);

    const skyColor = 0xB1E1FF;
    const groundColor = 0xB97A20;
    const light = new HemisphereLight(skyColor, groundColor, 1);
    temple.add(light);

    loadTemple().then((gltf) => {
      temple.add(gltf.scene);
    }).catch(console.error);

    renderer = new WebGLRenderer({ canvas, alpha: true });
    renderer.setSize(window.innerWidth, window.innerHeight)
  }, []);

  // animate camera
  useEffect(() => {
    const animate = () => {
      const animation = requestAnimationFrame(animate);
      camera.position.lerp(targetVector.current, ANIMATION_SPEED)
      camera.rotation.x = lerp(camera.rotation.x, targetRotation.current.x, ANIMATION_SPEED);
      camera.rotation.y = lerp(camera.rotation.y, targetRotation.current.y, ANIMATION_SPEED);
      camera.rotation.z = lerp(camera.rotation.z, targetRotation.current.z, ANIMATION_SPEED);

      if (camera.position !== targetVector.current) {
        updateRaycast()
      }
      renderer.render(temple, camera);
      return animation;
    }
    const animation = animate()
    return () => {
      cancelAnimationFrame(animation)
    }
  }, [])

  // handle mouse
  useEffect(() => {
    const onMouseMove = (event: MouseEvent) => {
      event.preventDefault();
      mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
      mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
      updateRaycast()
    }
    const onMouseDown = (event: MouseEvent) => {
      if (event.button !== 0) return
      const intersects = raycaster.intersectObjects(temple.children);

      if (intersects.length > 0) {
        switch (intersects[0].object.name) {
          case 'Cube2':
            targetVector.current = new Vector3(-40, 5, 0)
            targetRotation.current = new Vector3(0, -Math.PI * .5, 0)
            return true;
          case 'Cube1':
            targetVector.current = new Vector3(40, 5, 0)
            targetRotation.current = new Vector3(0, Math.PI * .5, 0)
            return true;
          case 'Cube3':
            targetVector.current = new Vector3(0, 5, 40)
            targetRotation.current = new Vector3(0, 0, 0)
            return true;
          case 'Cube4':
            targetVector.current = new Vector3(0, 5, -40)
            targetRotation.current = new Vector3(0, Math.PI, 0)
            return true;
          default:
            return false;

        }
      }
    }
    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mousedown', onMouseDown);
    return () => {
      document.removeEventListener('mousemove', onMouseMove);
    }
  }, [])

  // handle resize
  useEffect(() => {
    const onWindowResize = () => {
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
      renderer.setSize(window.innerWidth, window.innerHeight);
    }
    document.addEventListener('resize', onWindowResize)
    return () => {
      document.removeEventListener('resize', onWindowResize)
    }
  }, [])


  const updateRaycast = () => {
    camera.updateMatrixWorld();
    raycaster.setFromCamera(mouse, camera);
    const intersects = raycaster.intersectObjects(temple.children)
    if (intersects.length > 0 && intersects[0].object.name.startsWith('Cube')) {
      const cube = intersects[0].object as Mesh;
      cube.material = new MeshPhongMaterial({ color: '#ddd' });
    } else {
      temple.children.filter(child => { return child.isObject3D && child.name.startsWith('Cube') }).forEach(cube => {
        (cube as Mesh).material = new MeshPhongMaterial({ color: '#8AC' });
      })
    }
  }

  return (<div>
    <canvas id="temple" height={200} width={200}></canvas>
  </div>);

}