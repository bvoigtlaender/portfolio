import { z, defineCollection } from 'astro:content';

const blogCollection = defineCollection({
  type: 'content',
  schema: z.object({
    title: z.string(),
    tags: z.array(z.string()),
    creationDate: z.date(),
    modifyDate: z.date(),
    language: z.enum(['de', 'en']),
    contentWarning: z.optional(z.string())
  })
});

export const collections = {
  'blog': blogCollection,
};