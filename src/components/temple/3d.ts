import { BoxGeometry, DoubleSide, Mesh, MeshPhongMaterial, NearestFilter, PlaneGeometry, RepeatWrapping, Texture, TextureLoader } from 'three';
import { type GLTF, GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';

export function createCube(size: number = 4, name: string = 'Cube') {
  const cubeGeo = new BoxGeometry(size, size, size);
  const cubeMat = new MeshPhongMaterial({ color: '#8AC' });
  const mesh = new Mesh(cubeGeo, cubeMat);
  mesh.name = name
  return mesh;
}

export function createPlane(texture: Texture, size: number) {
  const planeGeo = new PlaneGeometry(size, size);
  const planeMat = new MeshPhongMaterial({
    map: texture,
    side: DoubleSide,
  });
  const mesh = new Mesh(planeGeo, planeMat);
  mesh.name = 'Plane'
  mesh.rotation.x = Math.PI * -.5;
  return mesh;
}

export function createTexture(size: number) {
  const loader = new TextureLoader();
  const texture = loader.load('/checker.png');
  texture.wrapS = RepeatWrapping;
  texture.wrapT = RepeatWrapping;
  texture.magFilter = NearestFilter;
  const repeats = size / 2;
  texture.repeat.set(repeats, repeats);
  return texture;
}

export function loadTemple(): Promise<GLTF> {
  return new Promise((resolve, reject) => {
    const loader = new GLTFLoader();
    loader.load('/temple.gltf', (gltf) => {
      resolve(gltf);
    }, (event) => {
      console.log(event.loaded)
    }, (error) => {
      reject(error)
    })
  })
}