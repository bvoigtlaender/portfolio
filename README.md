### Bjarne Voigtländer
[![status-badge](https://ci.bvoigtlaender.de/api/badges/bvoigtlaender/portfolio/status.svg)](https://ci.bvoigtlaender.de/bvoigtlaender/portfolio)

## Temple Todo
- Custom properties for 3d arrows for cam paths and points maybe also exported from blender.
- Create a better temple!
- Environment.

## Resume Todo
- Todo list for future endevours (how do you write that?!?!)
- Link to boring pdf && make it printable.
- Reference.
- Interests and things which are important to me.
- Tidy things up!